//
//  ChannelData+CoreDataProperties.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/26/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ChannelData {

    @NSManaged var channelindex: NSNumber?
    @NSManaged var datapoints: NSOrderedSet?
    @NSManaged var telemetry: TelemetryData?

}
