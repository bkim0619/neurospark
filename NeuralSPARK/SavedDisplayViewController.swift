//
//  SavedDisplayViewController.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/26/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//

import UIKit
import Charts
import MessageUI

class SavedDisplayViewController: UIViewController, UIPopoverPresentationControllerDelegate, MFMailComposeViewControllerDelegate {

    var model:TelemetryData!
    
    let xFormatter = XAxisFormatter()
    let yFormatter = YAxisFormatter()
    
    let displaySamplingFrequency = 250.0
    let expectedMaximum = 9.9951
    let expectedMinimum = -2.7832
    let MCUSamplingFrequency = Double(10000)

    
    @IBOutlet var lineCharts: [LineChartView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.whiteColor()
        
//        // choose which channels to display
//        let button = UIButton()
//        button.setImage(UIImage(named: "RadioFilled"), forState: .Normal)
//        button.frame = CGRectMake(0, 0, 30, 30)
//        button.addTarget(self, action: #selector(SavedDisplayViewController.action), forControlEvents: .TouchUpInside)
//        // set bar button item
//        let barButtonItem = UIBarButtonItem()
//        barButtonItem.customView = button
//        //barButtonItem.tintColor = UIColor(red: 0x4f/255.0, green: 0x8b/255.0, blue: 0x60/255.0, alpha: 1)
//        
//        // send e-mail bar button item
//        let sendButton = UIButton()
//        sendButton.setImage(UIImage(named:"SendFilled"), forState: .Normal)
//        sendButton.frame = CGRectMake(0, 0, 30, 30)
//        sendButton.addTarget(self, action: #selector(SavedDisplayViewController.send), forControlEvents: .TouchUpInside)
//        let sendBarButtonItem = UIBarButtonItem()
//        sendBarButtonItem.customView = sendButton
//        
//        self.navigationItem.setRightBarButtonItems([sendBarButtonItem, barButtonItem], animated:true)
        
        
        for lc in lineCharts {
            //            lc.backgroundColor = UIColor.lightGrayColor()
            lc.descriptionText = ""
            lc.xAxis.labelPosition = .Bottom
            lc.xAxis.drawLabelsEnabled = true
            lc.xAxis.drawGridLinesEnabled = false
            lc.leftAxis.drawGridLinesEnabled = false
            lc.rightAxis.drawGridLinesEnabled = false
            
            lc.xAxis.valueFormatter = xFormatter
            lc.leftAxis.valueFormatter = yFormatter
            lc.rightAxis.valueFormatter = yFormatter
        }
        
        initGraph()
    }
    
    func initGraph() {
        
        let savedSamplingFrequency = Double(model.samplingfrequency!.intValue)
        
        var orderedDisplayIndices = Saved.getChannelDisplayIndices(model.timestamp!)
        if orderedDisplayIndices[0] > orderedDisplayIndices[1] {
            let tmp = orderedDisplayIndices[0]
            orderedDisplayIndices[0] = orderedDisplayIndices[1]
            orderedDisplayIndices[1] = tmp
        }
        
        let baseChannel = model.basechannel
        
        let skipFactor = Int(model.samplingfrequency!.doubleValue / displaySamplingFrequency)
        
        for i in 0..<orderedDisplayIndices.count {
            var xs = [Double]()
            var ys = [Double]()
            
            let currChannelIndex = Int(baseChannel!.intValue) + orderedDisplayIndices[i]

            var channelIndex = 0
            for cd in model.channels! {
                
                let channelData = cd as! ChannelData
                
                if channelIndex == orderedDisplayIndices[i] {
                    var pointIndex = 0
                    for d in channelData.datapoints! {
                    
                        if pointIndex % skipFactor == 0 {
                            let dataPoint = d as! ChannelDataPoint
                            xs.append(Double(dataPoint.x!) / MCUSamplingFrequency)
                            ys.append(Double(dataPoint.y!) / 1023.0 * (expectedMaximum - expectedMinimum) + expectedMinimum)
                        }
                        
                        pointIndex += 1
                    }
                }
                
                channelIndex += 1
            }
            
            setChart(&xs, ys: &ys, chartPosition: i, channelIndex: currChannelIndex)
        }
    }

    func setChart(inout xs: [Double],inout ys: [Double], chartPosition:Int, channelIndex:Int) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<xs.count {
            let dataEntry = ChartDataEntry(x: xs[i] , y: ys[i])
            dataEntries.append(dataEntry)
        }
        
        let red = Double(0x34) // 00, 9e, 60
        let green = Double(0x84)
        let blue = Double(0xbf)
        let color = UIColor(red: CGFloat(red/255.0), green: CGFloat(green/255.0), blue: CGFloat(blue/255.0), alpha: 1)
        
        var colors: [UIColor] = []
        colors.append(color)
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Channel \(channelIndex+1) Neural Response (mV)")
        lineChartDataSet.colors = colors
        lineChartDataSet.drawCirclesEnabled = false
        
        let lineChartData = LineChartData(dataSets: [lineChartDataSet])
        lineChartData.setDrawValues(false)
        
        lineCharts[chartPosition].data = lineChartData
    }
    

    @IBAction func sendEmail(sender: UIBarButtonItem) {
        let mailString = NSMutableString()
        
        let baseChannel = model.basechannel!
        
        var channelOffset = 0
        for cd in model.channels! {
            
            let channelData = cd as! ChannelData
            
            mailString.appendString("Channel \(baseChannel.intValue + channelOffset)\n")
            
            for d in channelData.datapoints! {
                let dataPoint = d as! ChannelDataPoint
                
                mailString.appendString("\(Double(dataPoint.x!)) \(Double(dataPoint.y!))\n")
            }
            
            channelOffset += 1
        }
        
        // Converting it to NSData.
        let data = mailString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        // Unwrapping the optional.
        if let content = data {
            
            let emailViewController = configuredMailComposeViewController(content)
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(emailViewController, animated: true, completion: nil)
            }
        }

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func action(sender: UIButton) {
        print("action")
        performSegueWithIdentifier("showChannelsToDisplay", sender: sender)
    }

    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func configuredMailComposeViewController(data:NSData) -> MFMailComposeViewController {
        let emailController = MFMailComposeViewController()
        emailController.mailComposeDelegate = self
        emailController.setSubject("Neural Recording CSV File")
        emailController.setMessageBody("", isHTML: false)
        
        // Attaching the .CSV file to the email.
        emailController.addAttachmentData(data, mimeType: "text/csv", fileName: "Data.csv")
        
        return emailController
    }

    func adaptivePresentationStyleForPresentationController(
        controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    func createChannelDisplayModel() -> Parameter {
        
        let minChannel = model.basechannel!.intValue
        let maxChannel = model.topchannel!.intValue
        
        var values = [String]()
        
        for i in minChannel...maxChannel {
            values.append("Channel \(i+1)")
        }
        
        return .Picker(type: .SavedChannelDisplay, values: values)
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        initGraph()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier != nil {
            switch segue.identifier! {
            case "showChannelsToDisplay":
                if let destination = segue.destinationViewController as? ChannelDisplayViewController {
                    if sender as? UIBarButtonItem != nil {
                        destination.navigationItem.title = "Channel Display"
                        if let ppc = destination.popoverPresentationController {
                            ppc.delegate = self
                        }
                    }
                    destination.model = createChannelDisplayModel()
                    destination.pageType = .Saved(date:model.timestamp!)
                }
            default:
                break
            }
        }
    }
}
