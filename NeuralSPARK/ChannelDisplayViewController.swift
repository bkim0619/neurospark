//
//  ChannelDisplayViewController.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/19/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//

import UIKit

class ChannelDisplayViewController: UITableViewController {

    var model:Parameter!
    var pageType:PageType = .Telemetry
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.whiteColor()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch model! {
        case .Picker(_, let values):
            return values.count
        }
    }
    
    
    override var preferredContentSize: CGSize {
        get {
            if tableView != nil && presentingViewController != nil {
                return tableView.sizeThatFits(presentingViewController!.view.bounds.size)
            } else {
                return super.preferredContentSize
            }
        }
        set {
            super.preferredContentSize = newValue
        }
    }
    

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("channelDisplayCell", forIndexPath: indexPath)

        switch model! {
        case .Picker(_, let values):
            cell.textLabel?.text = values[indexPath.row]
            
            var channelIndices = [-1, -1]
            switch(pageType) {
            case .Telemetry:
                channelIndices = Telemetry.channelDisplayIndices
                break;
            case .Saved(let date):
                channelIndices = Saved.getChannelDisplayIndices(date)
                break;
            default:
                break;
            }
            
            if indexPath.row == channelIndices[0] || indexPath.row == channelIndices[1] {
                cell.accessoryType = .Checkmark
            } else {
                cell.accessoryType = .None
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView.cellForRowAtIndexPath(indexPath) != nil {
            
            switch pageType {
            case .Telemetry:
                var channelIndices = Telemetry.channelDisplayIndices
                
                if indexPath.row != channelIndices[0] && indexPath.row != channelIndices[1] {
                    //cell.accessoryType = .Checkmark
                    
                    channelIndices[0] = channelIndices[1]
                    channelIndices[1] = indexPath.row
                    
                    Telemetry.channelDisplayIndices = channelIndices
                    
                    tableView.reloadData()
                }
                break
            case .Saved(let date):
                var channelIndices = Saved.getChannelDisplayIndices(date)
                
                if indexPath.row != channelIndices[0] && indexPath.row != channelIndices[1] {
                    //cell.accessoryType = .Checkmark
                    
                    channelIndices[0] = channelIndices[1]
                    channelIndices[1] = indexPath.row
                    
                    Saved.setChannelDisplayIndices(date, newValue: channelIndices)
                    
                    tableView.reloadData()
                }
                break
            default:
                break
            }

        }
    }
    
    /*
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            
            var channelIndices = Telemetry.channelDisplayIndices
            
            if indexPath.row == channelIndices[0] || indexPath.row == channelIndices[1] {
                cell.accessoryType = .None
            }
        }
    }*/

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
