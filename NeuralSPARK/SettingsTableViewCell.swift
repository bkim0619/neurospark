//
//  PickerTableViewCell.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/15/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var detail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
