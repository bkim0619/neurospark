//
//  ChannelTableViewController.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/15/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//

import UIKit

class StimulationTableViewController: UITableViewController {

    @IBOutlet weak var globalSettingsButton: UIBarButtonItem!
    
    @IBOutlet weak var stimulateButton: UIBarButtonItem!
    
    var preferences = NSUserDefaults.standardUserDefaults()
    
    var numOfChannelsPerRow = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.whiteColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
    }
    
    

    func createStimulationMessage() -> String {
        var message = ""
        
        let periodIndex = Util.getSelectedIndex(.Global, parameterType: .Period)
        let period = Util.periods[periodIndex]
        
        let maxWidthIndex = Util.getSelectedIndex(.Global, parameterType: .MaxWidth)
        let maxAmpIndex = Util.getSelectedIndex(.Global, parameterType: .MaxAmplitude)
        
        let divider = Util.phaseOptions[maxWidthIndex].divider
        let widthGainIndex = Util.phaseOptions[maxWidthIndex].widthGainIndex
        let vgcmIndex = Util.ampOptions[maxAmpIndex].vgcmIndex
        

        message += "\(period);\(divider);\(widthGainIndex);\(vgcmIndex);"
        
        for i in 0..<Util.numOfStimulationChannels {
            
            let channelEnum = PageType.Channel(index: i)
            
            let anodicAmpIndex = Util.getSelectedIndex(channelEnum, parameterType: .AnodicAmplitude)
            let cathodicAmpIndex = Util.getSelectedIndex(channelEnum, parameterType: .CathodicAmplitude)
            let anodicWidthIndex = Util.getSelectedIndex(channelEnum, parameterType: .AnodicWidth)
            let cathodicWidthIndex = Util.getSelectedIndex(channelEnum, parameterType: .CathodicWidth)
            let startingDelayIndex = Util.getSelectedIndex(channelEnum, parameterType: .StartDelay)
            let interphaseDelayIndex = Util.getSelectedIndex(channelEnum, parameterType: .InterphaseDelay)
            let reversePolarityIndex = Util.getSelectedIndex(channelEnum, parameterType: .ReversePolarity)
            let cancelChargeIndex = Util.getSelectedIndex(channelEnum, parameterType: .CancelCharge)
            
            if anodicAmpIndex > 0 || cathodicAmpIndex > 0 {
                message += "\(i);\(anodicAmpIndex);\(cathodicAmpIndex);\(anodicWidthIndex);\(cathodicWidthIndex);\(startingDelayIndex);\(interphaseDelayIndex);\(reversePolarityIndex);\(cancelChargeIndex);"
            }
        }
        
        message += " "
        
        print(message)
        
        return message
    }
    
    @IBAction func stimulate(sender: UIBarButtonItem) {
        
        var (success, errmsg) = Util.client.connect(timeout: 3)
        
        if success {
            let message = createStimulationMessage()
            
            (success, errmsg) = Util.client.send(str:message)
            
            if success {
//                var data = Util.client.read(1024*10)
//                if data != nil {
//                    for i in 0..<data!.count {
//                        print(Character(UnicodeScalar(data![i])))
//                    }
//                }
            } else {
                print(errmsg)
            }
        } else {
            print(errmsg)
        }
        
        Util.client.close()
        
        var alertMessage = ""
        if (success) {
            alertMessage = "Stimulation command sent"
        } else {
            alertMessage = "Unable to connect to rendezvous device. Please try again later."
        }
        
        let alertController = UIAlertController(title: "", message:
            alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }

    @IBAction func showGlobalSettings(sender: UIBarButtonItem) {
        performSegueWithIdentifier("showGlobalSettings", sender: sender)
    }
    
    @IBAction func selectChannelSettings(sender:UIButton) {
        performSegueWithIdentifier("showChannelSettings", sender: sender)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Util.numOfStimulationChannels / numOfChannelsPerRow
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("stimulationChannelCell", forIndexPath: indexPath)
        
        if let stimulationChannelCell = cell as? StimulationTableViewCell {
            if let buttons = stimulationChannelCell.buttons {
                var channelIndex = indexPath.row * numOfChannelsPerRow

                for button in buttons {
                    // button.layer.cornerRadius = 2
                    //            button.layer.borderWidth = 1
                    //            button.layer.borderColor = UIColor.blackColor().CGColor
                    button.setTitle("\(channelIndex + 1)", forState: .Normal)
                    button.tag = channelIndex
                    
                    if Util.getSelectedIndex(.Channel(index: channelIndex), parameterType: .AnodicAmplitude) > 0 || Util.getSelectedIndex(.Channel(index: channelIndex), parameterType: .CathodicAmplitude) > 0 {
                    
                        button.setTitleColor(UIColor(red: 0x34/255.0, green: 0x84/255.0, blue: 0xbf/255.0, alpha: 1), forState:.Normal)
                        button.backgroundColor = UIColor(red: 0xfe/255.0, green: 0xbb/255.0, blue: 0x19/255.0, alpha: 1)
                        
                    } else {
                        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                        button.backgroundColor = UIColor(red: 0x34/255.0, green: 0x84/255.0, blue: 0xbf/255.0, alpha: 1)
                    }
                    
                    channelIndex += 1
                }
            }
        }
        

        return cell
    }


    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }


    func createChannelModel() -> [Parameter] {
        var toReturn = [Parameter]()
        
        // anodic amp
        // get selected index for global max amp
        let maxAmpKey = Util.getKey(.Global, parameterType: .MaxAmplitude)
        let maxAmpSelectedIndex = preferences.integerForKey(maxAmpKey) ?? 0
        let anodicAmps = Util.ampOptions[maxAmpSelectedIndex].microAmps
        let anodicAmpValues = anodicAmps.map {"\($0) uA"}
        let anodicAmpParameter = Parameter.Picker(type: .AnodicAmplitude, values: anodicAmpValues)
        toReturn.append(anodicAmpParameter)
        
        
        // cathodic amp
        let cathodicAmpValues = anodicAmps.map {"\($0) uA"}
        let cathodicAmpParameter = Parameter.Picker(type:.CathodicAmplitude, values:cathodicAmpValues)
        toReturn.append(cathodicAmpParameter)
        
        let formatter = NSNumberFormatter()
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 3
        formatter.minimumIntegerDigits = 1
        
        // anodic width
        let maxWidthKey = Util.getKey(.Global, parameterType:.MaxWidth)
        let maxWidthSelectedIndex = preferences.integerForKey(maxWidthKey) ?? 0
        let anodicWidths = Util.phaseOptions[maxWidthSelectedIndex].phaseWidths
        let anodicWidthValues = anodicWidths.map{"\(formatter.stringFromNumber($0) ?? "0") ms"}
        let anodicWidthParameter = Parameter.Picker(type: .AnodicWidth, values: anodicWidthValues)
        toReturn.append(anodicWidthParameter)
        
        
        // cathodic width
        let cathodicWidthValues = anodicWidths.map{"\(formatter.stringFromNumber($0) ?? "0") ms"}
        let cathodicWidthParameter = Parameter.Picker(type: .CathodicWidth, values: cathodicWidthValues)
        toReturn.append(cathodicWidthParameter)
        
        
        // start delay
        let startDelays = Util.phaseOptions[maxWidthSelectedIndex].startDelays
        let startDelayValues = startDelays.map {"\(formatter.stringFromNumber($0) ?? "0") ms"}
        let startDelayParameter = Parameter.Picker(type: .StartDelay, values: startDelayValues)
        toReturn.append(startDelayParameter)
        
        
        // interphase delay
        let interphaseDelays = Util.phaseOptions[maxWidthSelectedIndex].interphaseDelays
        let interphaseDelayValues = interphaseDelays.map{"\(formatter.stringFromNumber($0) ?? "0") ms"}
        let interphaseDelayParameter = Parameter.Picker(type: .InterphaseDelay, values: interphaseDelayValues)
        toReturn.append(interphaseDelayParameter)
        
        
        // reverse polarity
        let reversePolarityValues = ["Off", "On"]
        let reversePolarityParameter = Parameter.Picker(type: .ReversePolarity, values: reversePolarityValues)
        toReturn.append(reversePolarityParameter)
        
        
        // cancel charge
        let cancelChargeValues = ["Off", "On"]
        let cancelChargeParameter = Parameter.Picker(type: .CancelCharge, values: cancelChargeValues)
        toReturn.append(cancelChargeParameter)
        
        
        return toReturn
    }
    
    func createGlobalModel() -> [Parameter] {
        var toReturn = [Parameter]()
        
        let periods = Util.periods
        let periodParameter = Parameter.Picker(type: .Period, values:periods.map {"\($0) ms"})
        
        toReturn.append(periodParameter)
        
        // create max amp parameters
        var maxAmps = [Int]()
        let amp = Amp()
        for i in 0..<amp.ampOptions.count {
            let ampOption = amp.ampOptions[i]
            maxAmps.append(ampOption.maxMicroAmp)
        }
        let maxAmpParameter = Parameter.Picker(type: .MaxAmplitude, values:maxAmps.map{"\($0) uA"})
        toReturn.append(maxAmpParameter)
        
        
        // create max width parameters
        let phase = Phase()
        var maxWidths = [Double]()
        for i in 0..<phase.phaseOptions.count {
            let phaseOption = phase.phaseOptions[i]
            maxWidths.append(phaseOption.maxPhaseWidth)
        }
        let maxPhaseParameter = Parameter.Picker(type: .MaxWidth, values:maxWidths.map{"\($0) ms"})
        toReturn.append(maxPhaseParameter)
        
        return toReturn
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier != nil {
            switch segue.identifier! {
            case "showGlobalSettings":
                if let destination = segue.destinationViewController as? SettingsTableViewController {
                    if (sender as? UIBarButtonItem) != nil {
                        destination.navigationItem.title = "Global Parameters"
                        destination.model = createGlobalModel()
                        destination.pageType = .Global
                    }
                }
                break
            case "showChannelSettings":
                
                if let destination = segue.destinationViewController as? ChannelSettingsViewController {
                    if let button = sender as? UIButton {
                        destination.navigationItem.title = "Channel \(button.tag + 1)"
                        destination.model = createChannelModel()
                        destination.pageType = .Channel(index:button.tag)
                    }
                }
                
                break
            default:
                break
                
            }
        }
    }

}
