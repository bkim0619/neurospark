//
//  WaveFormView.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/18/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//

import UIKit

class WaveformView: UIView {

    let lineWidth = CGFloat(3)
    let windowPercentage = CGFloat(0.9)

    var pageType:PageType!
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        let amps = Util.getSelectedAmplitudes(pageType)

        let midX = bounds.width / 2
        let midY = bounds.height / 2
        
        let startX = bounds.width * (1 - windowPercentage) / 2
        let startY = midY

        let endX = bounds.width - bounds.width * (1 - windowPercentage) / 2
        let endY = midY
        
        var points = [CGPoint]()
        
        if amps.anodic == 0 && amps.cathodic == 0 {
            
            points.append(CGPoint(x:startX, y:startY))
            points.append(CGPoint(x:endX, y:endY))
            
        } else {
            let widths = Util.getSelectedWidths(pageType)
            
            let windowHeight = bounds.height * windowPercentage
            let windowWidth = bounds.width * windowPercentage
            
            let pulseMaxHeight = windowHeight / 2
            let pulseMaxWidth = windowWidth / 4
            
            var anodicHeight = pulseMaxHeight * CGFloat(amps.anodic) / CGFloat(amps.max)
            var cathodicHeight = pulseMaxHeight * CGFloat(amps.cathodic) / CGFloat(amps.max)
            var anodicWidth = pulseMaxWidth * CGFloat(widths.anodic) / CGFloat(widths.max)
            var cathodicWidth = pulseMaxWidth * CGFloat(widths.cathodic) / CGFloat(widths.max)
            let interphaseWidth = pulseMaxWidth * CGFloat(widths.interphase) / CGFloat(widths.max)
            let interphaseHalf = interphaseWidth / 2

            if Util.getSelectedIndex(pageType, parameterType: .ReversePolarity) != 0 {
                var tmp = anodicHeight
                anodicHeight = cathodicHeight
                cathodicHeight = tmp
                
                tmp = anodicWidth
                anodicWidth = cathodicWidth
                cathodicWidth = tmp
            }
            
            points.append(CGPoint(x: startX, y: startY))
            
            points.append(CGPoint(x:midX - interphaseHalf - anodicWidth, y:midY))
            points.append(CGPoint(x:midX - interphaseHalf - anodicWidth, y:midY - anodicHeight))
            points.append(CGPoint(x:midX - interphaseHalf, y:midY - anodicHeight))
            
            if interphaseHalf > 0 {
                points.append(CGPoint(x:midX - interphaseHalf, y:midY)) // first interphase
                points.append(CGPoint(x:midX + interphaseHalf, y:midY)) // second interphase
            }
            
            points.append(CGPoint(x:midX + interphaseHalf, y:midY + cathodicHeight))
            points.append(CGPoint(x:midX + interphaseHalf + cathodicWidth, y:midY + cathodicHeight))
            points.append(CGPoint(x:midX + interphaseHalf + cathodicWidth, y:midY))
            
            points.append(CGPoint(x:endX, y:endY))
        }
        
        let path = UIBezierPath()
        
        path.moveToPoint(points[0])
        
        for i in 1..<points.count {
            path.addLineToPoint(points[i])
        }

        UIColor.redColor().set()
        path.lineWidth = lineWidth
        path.stroke()
    }
}
