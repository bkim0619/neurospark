//
//  SettingsTableViewController.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/15/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    var pageType:PageType!
    var model:[Parameter]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.whiteColor()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
    }

    @IBAction func resetSettings(sender: UIButton) {
        print("reset settings")
    }
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (model.count)
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row < model.count {
            let cell = tableView.dequeueReusableCellWithIdentifier("settingsCell", forIndexPath: indexPath)
            
            if let pickerCell = cell as? SettingsTableViewCell {
                
                pickerCell.tag = indexPath.row
                let parameter = model[indexPath.row]
                
                switch parameter {
                case .Picker(let type, let values):
                    pickerCell.label.text = type.description
                    
                    let selectedIndex = Util.getSelectedIndex(pageType, parameterType: type)
                    
                    pickerCell.detail.text = values[selectedIndex]
                    break
                }
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("resetCell", forIndexPath: indexPath)
            return cell
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier != nil {
            switch segue.identifier! {
            case "showPickerValues":
                if let destination = segue.destinationViewController as? PickerValuesTableViewController {
                    if let tableViewCell = sender as? SettingsTableViewCell {
                        let row = tableViewCell.tag
                        
                        switch model[row] {
                        case .Picker(let type, _):
                            destination.navigationItem.title = type.description
                        }
                        destination.model = model[row]
                        destination.pageType = pageType
                    }
                }
                break
            default:
                break
            }
        }
    }

}
