//
//  SavedTableViewController.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/26/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//

import UIKit
import CoreData

class SavedTableViewController: UITableViewController {

    var model:[TelemetryData]!
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    let formatter = NSDateFormatter()
    let shortFormatter = NSDateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Util.setBackgroundColor(view)
        view.backgroundColor = UIColor.whiteColor()
        
        self.title = "Saved"
        
        //formatter.dateStyle = .LongStyle
        formatter.dateStyle = .ShortStyle
        formatter.timeStyle = .MediumStyle
        
//        shortFormatter.dateFormat = "M/d/y at h:mm:ss a"
        shortFormatter.dateStyle = .ShortStyle
        shortFormatter.timeStyle = .MediumStyle
        
    }

    override func viewWillAppear(animated: Bool) {
        loadData()
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadData() {
        let fetchRequest = NSFetchRequest()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entityForName("TelemetryData", inManagedObjectContext: self.managedObjectContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        let sectionSortDescriptor = NSSortDescriptor(key: "timestamp", ascending: false)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        do {
            let result = try self.managedObjectContext.executeFetchRequest(fetchRequest)
            model = result as! [TelemetryData]
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
    
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("savedCell", forIndexPath: indexPath)

        let telemetryData = model[indexPath.row]

        // Configure the cell...
        cell.textLabel?.text = formatter.stringFromDate(telemetryData.timestamp!)
        cell.detailTextLabel?.text = "Ch. \(telemetryData.basechannel!.intValue + 1)-\(telemetryData.topchannel!.intValue + 1)"
        cell.tag = indexPath.row
        
        return cell
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    
        if segue.identifier != nil {
            switch segue.identifier! {
            case "showTelemetryData":
                if let destination = segue.destinationViewController as? SavedDisplayViewController {
                    if let cell = sender as? UITableViewCell {
                        
                        let modelToSend = model[cell.tag]
                        
                        destination.navigationItem.title = shortFormatter.stringFromDate(modelToSend.timestamp!)
                        destination.model = modelToSend
                    }
                }
            default:
                break
            }
        }
    
        // set controller title
    }

}
