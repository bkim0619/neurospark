//
//  Enums.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/15/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//

import Foundation
import UIKit
import Charts

enum Parameter {
    case Picker(type:ParameterType, values:[String])
}

enum PageType {
    case Global
    case Telemetry
    case Channel(index:Int)
    case Saved(date:NSDate)
    
    var description: String {
    
        switch(self) {
        case .Global:
            return "Global"
        case .Telemetry:
            return "Telemetry"
        case .Channel(let index):
            return "Channel\(index)"
        case .Saved(let date):
            return "Saved at \(date)"
        }
    }
}

enum ParameterType {
    // global
    case Period
    case MaxAmplitude
    case MaxWidth
    // channel
    case AnodicAmplitude
    case CathodicAmplitude
    case AnodicWidth
    case CathodicWidth
    case StartDelay
    case InterphaseDelay
    case ReversePolarity
    case CancelCharge
    
    // telemetry
    case WindowSize
    case ChannelSource
    case SamplingFrequency
    case ChannelDisplay
    case DisplayMode
    
    // saved
    case SavedChannelDisplay
    
    var description: String {
        switch(self) {
        case .Period:
            return "Period"
        case .MaxAmplitude:
            return "Max Amplitude"
        case .MaxWidth:
            return "Max Width"
        case .AnodicAmplitude:
            return "Anodic Amplitude"
        case .CathodicAmplitude:
            return "Cathodic Amplitude"
        case .AnodicWidth:
            return "Anodic Width"
        case .CathodicWidth:
            return "Cathodic Width"
        case .StartDelay:
            return "Start Delay"
        case .InterphaseDelay:
            return "Interphase Delay"
        case .ReversePolarity:
            return "Reverse Polarity"
        case .CancelCharge:
            return "Cancel Charge"
        case .WindowSize:
            return "Window Size"
        case .ChannelSource:
            return "Channel Source"
        case .SamplingFrequency:
            return "Sampling Frequency"
        case .ChannelDisplay:
            return "Channel Display"
        case .DisplayMode:
            return "Display Mode"
        case .SavedChannelDisplay:
            return "Saved Channel Display"
        }
    }
}

struct PhaseOption {
    var divider:Int
    var widthGain:Int
    var widthGainIndex:Int
    
    var maxPhaseWidth:Double
    var phaseWidths:[Double]
    var startDelays:[Double]
    var interphaseDelays:[Double]
}

class Phase {
    var PLL = 2e6
    var widthGains = [1, 2, 4, 8]
    var dividers = [7, 9, 19, 39, 79, 99, 199, 249]
    var divider = 249
    var widthGainCode = 0
    var multiplier = 0
    var phaseOptions = [PhaseOption]()
    
    init() {
        var maxPhaseWidthSet = Set<Double>()
        for divider in dividers {
            for i in 0..<widthGains.count {
                let widthGain = widthGains[i]
                let maxPhaseWidth = getMaxPhaseWidth(divider, widthGain)
                
                if maxPhaseWidthSet.contains(maxPhaseWidth) {
                    continue
                }
                
                maxPhaseWidthSet.insert(maxPhaseWidth)
                var phaseWidths = [Double]()
                for multiplier in 0..<8 {
                    phaseWidths.append(getPhaseWidth(divider, widthGain, multiplier))
                }
                phaseWidths.sortInPlace()
                
                var startDelays = [Double]()
                for multiplier in 0..<256 {
                    startDelays.append(getStartDelay(divider, widthGain, multiplier))
                }
                
                var interphaseDelays = [Double]()
                for multiplier in 0..<8 {
                    interphaseDelays.append(getInterphaseDelay(divider, widthGain, multiplier))
                }
                
                phaseOptions.append(PhaseOption(divider: divider, widthGain: widthGain, widthGainIndex: i, maxPhaseWidth: maxPhaseWidth, phaseWidths: phaseWidths, startDelays: startDelays, interphaseDelays: interphaseDelays))
            }
        }
        
        phaseOptions.sortInPlace({ $0.maxPhaseWidth < $1.maxPhaseWidth })
        
    }
    
    static func getPeriods() -> [Int] {
        // create period parameters
        var periods = [Int]()
        let basePeriod = 10 // ms
        let increment = 10
        let numOfPeriods = 11
        let period = basePeriod
        for i in 0..<numOfPeriods {
            periods.append(period + i * increment)
        }
        
        return periods
    }
    
    func getStartDelay(divider:Int, _ widthGain:Int, _ multiplier:Int) -> Double { // in ms
        let delay = 1.0 * Double(multiplier) * 1000 * 2 * Double(divider + 1) / self.PLL
        return Double(round(1000*delay)/1000)
    }
    
    func getInterphaseDelay(divider:Int, _ widthGain:Int, _ multiplier:Int) -> Double {
        return getValueInMS(divider, widthGain, multiplier)
    }
    
    func getPhaseWidth(divider:Int, _ widthGain:Int, _ multiplier:Int) -> Double {
        var multiplierFactor = multiplier
        if multiplier == 0 {
            multiplierFactor = 8
        }
        let value = getWidth(divider, widthGain) * Double(multiplierFactor) * 1000
        return Double(round(1000*value)/1000)
    }
    
    func getMaxPhaseWidth(divider:Int, _ widthGain:Int) -> Double {
        let maxPhase = getWidth(divider, widthGain) * 8 * 1000
        return Double(round(1000*maxPhase)/1000)
    }
    
    func getWidth(divider:Int, _ widthGain:Int) -> Double {
        return Double(divider + 1) * Double(widthGain) / self.PLL
    }
    
    func getValueInMS(divider:Int, _ widthGain:Int, _ multiplier:Int) -> Double {
        let width = getWidth(divider, widthGain)
        let value = width * Double(multiplier) * 1000
        return Double(round(1000*value)/1000)
    }
}

class Saved {
    static func getChannelDisplayIndices(date:NSDate) -> [Int] {
        let key = Util.getKey(.Saved(date: date), parameterType: .SavedChannelDisplay)
        return Util.preferences.objectForKey(key) as? [Int] ?? [0,1]
    }
    
    static func setChannelDisplayIndices(date:NSDate, newValue:[Int]) {
        let key = Util.getKey(.Saved(date: date), parameterType: .SavedChannelDisplay)
        Util.preferences.setObject(newValue, forKey: key)
    }
}

class Telemetry {

    static let windowSizes = [1, 5, 10, 30, 60] // in seconds
    static let samplingFrequencies = [1000, 2500, 5000, 10000] // in Hz
    static let maxSamplingFrequency = 10000
    static let channelSources = [(160, 175), (160, 167), (168, 175), (160, 163), (164, 167), (168, 171), (172, 174)]
    
    
    static var displayModeIndex: Int {
        return Util.getSelectedIndex(.Telemetry, parameterType: .DisplayMode)
    }
    
    static var numOfChannels: Int {
        get {
            return (channelSource.1 - channelSource.0 + 1)
        }
    }
    
    static var channelSource: (Int, Int) { // selected channel source
        get {
            let index = Util.preferences.integerForKey(Util.getKey(.Telemetry, parameterType: .ChannelSource))
            return channelSources[index]
        }
    }
    
    static var windowSize: Int {
        get {
            let index = Util.getSelectedIndex(.Telemetry, parameterType: .WindowSize)
            return windowSizes[index]
        }
    }
    
    static var samplingFrequency: Int {
        get {
            let index = Util.getSelectedIndex(.Telemetry, parameterType: .SamplingFrequency)
            return samplingFrequencies[index]
        }
    }
    
    static var channelSourceIndex: Int {
        get {
            return Util.preferences.integerForKey(Util.getKey(.Telemetry, parameterType: .ChannelSource))
        }
    }
    
    static var channelDisplayIndices:[Int] { // selected display indices
        get {
            return Util.preferences.objectForKey(Util.getKey(.Telemetry, parameterType: .ChannelDisplay)) as? [Int] ?? [0,1]
        }
        set {
            Util.preferences.setObject(newValue, forKey: Util.getKey(.Telemetry, parameterType: .ChannelDisplay))
        }
    }
    
    static var channelDisplays:(Int,Int) {
        get {
            let displayIndices = channelDisplayIndices
            let sourceIndex = channelSourceIndex
            
            let firstChannel = channelSources[sourceIndex].0 + displayIndices[0]
            let secondChannel = channelSources[sourceIndex].0 + displayIndices[1]
            
            return (firstChannel, secondChannel)
        }
    }
}


struct AmpOption {
    var vgcmValue:Int
    var vgcmIndex:Int
    var maxMicroAmp:Int
    var microAmps:[Int]
}

class Amp {
    var vgcmValues = [1, 2, 3, 5, 6, 7, 8, 10]
    var dacStep = 10.0/3
    var maxDacBit = 15
    var ampOptions = [AmpOption]()
    
    init() {
        for i in 0..<vgcmValues.count {
            let vgcmValue = vgcmValues[i]
            let maxMicroAmp = getMaxMicroAmp(vgcmValue)
            var microAmps = [Int]()
            for j in 0...maxDacBit {
                microAmps.append(getMicroAmp(vgcmValue, j))
            }
            
            ampOptions.append(AmpOption(vgcmValue: vgcmValue, vgcmIndex: i, maxMicroAmp: maxMicroAmp, microAmps: microAmps))
        }
    }
    
    func getMicroAmp(vgcmValue:Int, _ dac:Int) -> Int {
        return Int(round(dacStep * Double(dac) * Double(vgcmValue)))
    }
    
    func getMaxMicroAmp(vgcmValue:Int) -> Int {
        return Int(round(dacStep * Double(maxDacBit) * Double(vgcmValue)))
    }
}

class Colors {
    
    let greyTop:CGFloat = 250.0
    let greyBottom:CGFloat = 200.0
    
    let gl: CAGradientLayer
    
    init() {
        let colorTop = UIColor(red: greyTop/255.0, green: greyTop/255.0, blue: greyTop/255.0, alpha: 1.0).CGColor
        let colorBottom = UIColor(red: greyBottom/255.0, green: greyBottom/255.0, blue: greyBottom/255.0, alpha: 1.0).CGColor
        
        gl = CAGradientLayer()
        gl.colors = [ colorTop, colorBottom]
        gl.locations = [ 0.0, 1.0]
    }
}


class YAxisFormatter: NSObject, IAxisValueFormatter {
    
    let formatter = NSNumberFormatter()
    
    override init() {
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 1
        formatter.minimumIntegerDigits = 1
    }
    
    @objc func stringForValue(value: Double, axis: AxisBase?) -> String {
        return formatter.stringFromNumber(value)!
    }
}

class XAxisFormatter: NSObject, IAxisValueFormatter {
    
    let formatter = NSNumberFormatter()
    
    override init() {
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.minimumIntegerDigits = 1
    }
    
    @objc func stringForValue(value: Double, axis: AxisBase?) -> String {
        return "\(formatter.stringFromNumber(value)!) s"
    }
}

class Util {
    
    static let preferences = NSUserDefaults.standardUserDefaults()
    
    static let backgroundColor = UIColor(red: CGFloat(200/255.0), green: CGFloat(200/255.0), blue: CGFloat(200/255.0), alpha: 1)
    static let navigationBackgroundColor = UIColor(red: CGFloat(250/255.0), green: CGFloat(250/255.0), blue: CGFloat(250/255.0), alpha: 1)
    
    static let bruinBlue = UIColor(red: CGFloat(0x34/255.0), green: CGFloat(0x84/255.0), blue: CGFloat(0xbf/255.0), alpha: 1)
    static let bruinGold = UIColor(red: CGFloat(0xfe/255.0), green: CGFloat(0xbb/255.0), blue: CGFloat(0x19/255.0), alpha: 1)
    
    static let numOfStimulationChannels = 160
    
    static let numOfRecordingChannels = 16
    
    static let channelParametersOrder : [ParameterType] = [.AnodicAmplitude, .CathodicAmplitude, .AnodicWidth, .CathodicWidth, .StartDelay, .InterphaseDelay, .ReversePolarity, .CancelCharge]
    
    static let ampOptions = Amp().ampOptions
    
    static let phaseOptions = Phase().phaseOptions
    
    static let periods = Phase.getPeriods()
    
    //static let server = TCPServer(addr:"164.67.24.43", port:8888)
    
    //static let client = TCPClient(addr: "164.67.24.41", port: 2048)
    //static let client = TCPClient(addr: "localhost", port: 8888)
    static let client = TCPClient(addr:"192.168.43.46", port:8888)
    
    static let colors = Colors()
    
//    static func setGradient() {
//        let gradientLayer = CAGradientLayer()
//        let greyTop = CGFloat(250.0)
//        let greyBottom = CGFloat(200.0)
//        let topColor = UIColor(red: greyTop/255.0, green: greyTop/255.0, blue: greyTop/255.0, alpha: 1.0)
//        let bottomColor = UIColor(red: greyBottom/255.0, green: greyBottom/255.0, blue: greyBottom/255.0, alpha: 1.0)
//        
//        gradientLayer.frame = navigationController!.navigationBar.bounds
//        gradientLayer.colors = [topColor, bottomColor].map{$0.CGColor}
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        //gradientLayer.locations = [0.0, 1.0]
//        
//        // Render the gradient to UIImage
//        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
//        gradientLayer.renderInContext(UIGraphicsGetCurrentContext()!)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        
//        // Set the UIImage as background property
//        navigationController!.navigationBar.setBackgroundImage(image, forBarMetrics: UIBarMetrics.Default)
//    }
    
    static func setBackgroundColor(view:UIView) {
        view.backgroundColor = UIColor.clearColor()
        let backgroundLayer = colors.gl
        backgroundLayer.frame = view.frame
        view.layer.insertSublayer(backgroundLayer, atIndex: 0)
    }
    
    static func getKey(pageType: PageType, parameterType: ParameterType) -> String {
        return "\(pageType.description.lowercaseString)_\(parameterType.description.lowercaseString)"
    }
    
    static func getSelectedIndex(pageType: PageType, parameterType: ParameterType) -> Int {
        
        let keyName = getKey(pageType, parameterType:parameterType)
        return preferences.integerForKey(keyName)
    }
    
    static func getSelectedAmplitudes(pageType:PageType) -> (max:Int, anodic:Int, cathodic:Int) {
        let maxIndex = getSelectedIndex(.Global, parameterType: .MaxAmplitude)
        let maxValue = ampOptions[maxIndex].maxMicroAmp
        
        let anodicIndex = getSelectedIndex(pageType, parameterType: .AnodicAmplitude)
        let cathodicIndex = getSelectedIndex(pageType, parameterType: .CathodicAmplitude)
        
        let anodicValue = ampOptions[maxIndex].microAmps[anodicIndex]
        let cathodicValue = ampOptions[maxIndex].microAmps[cathodicIndex]
        
        return (maxValue, anodicValue, cathodicValue)
    }
    
    static func getSelectedWidths(pageType:PageType) -> (max:Double, anodic:Double, cathodic:Double, start:Double, interphase:Double) {
        let maxIndex = getSelectedIndex(.Global, parameterType: .MaxWidth)
        let maxValue = phaseOptions[maxIndex].maxPhaseWidth
        
        let anodicIndex = getSelectedIndex(pageType, parameterType: .AnodicWidth)
        let cathodicIndex = getSelectedIndex(pageType, parameterType: .CathodicWidth)
        let startDelayIndex = getSelectedIndex(pageType, parameterType: .StartDelay)
        let interphaseDelayIndex = getSelectedIndex(pageType, parameterType: .InterphaseDelay)
        
        let anodicValue = phaseOptions[maxIndex].phaseWidths[anodicIndex]
        let cathodicValue = phaseOptions[maxIndex].phaseWidths[cathodicIndex]
        let startDelayValue = phaseOptions[maxIndex].startDelays[startDelayIndex]
        let interphaseDelayValue = phaseOptions[maxIndex].interphaseDelays[interphaseDelayIndex]
        
        return (maxValue, anodicValue, cathodicValue, startDelayValue, interphaseDelayValue)
    }
}
