//
//  TelemetryViewController.swift
//  NeuralSPARK
//
//  Created by Brian Kim on 8/18/16.
//  Copyright © 2016 Brian Kim. All rights reserved.
//

import UIKit
import Charts
import CoreData

class TelemetryViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet var lineCharts: [LineChartView]!
    
    @IBOutlet weak var startButton: UIBarButtonItem!
    
    // Retreive the managedObjectContext from AppDelegate
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    let oversamplingFactor = 2
    let numOfBitsPerSample = 12
    let headerMarker:UInt16 = 0x555
    let samplingFrequencyMCU = 10000
    let refreshesPerSecond = 10
    let expectedMaximum = 9.9951
    let expectedMinimum = -2.7832
    
    let displayMinimum = -2.0
    let displayMaximum = 2.0
    
    var displayInterval:Int {
        get {
            return samplingFrequencyMCU/refreshesPerSecond * Telemetry.numOfChannels
        }
    }
    
    let xFormatter = XAxisFormatter()
    let yFormatter = YAxisFormatter()
    
    let displaySamplingFrequency = 1000 // number of points to display per second in the chart view
    let numOfCharts = 2
    
    var streaming = false
    var recording = false
    
    var rawBitStreamIndex = 0 // how much has been processed
    var rawBitStream = [UInt8]()
    
    var decodedBitStreamIndex = 0 // where to add next bit
    var decodedIndex = 0 // how much has been converted into a value
    var decodedBitStream = [UInt8]()
    
    var displayIndex = 0
    var dataSamples = [UInt16]()
    var needToSave = false
    
    var saveIndex = 0
    var saveInterval:Int {
        get {
            return 10000 * Telemetry.numOfChannels
        }
    }
    
    var xValues = [[Double]]()
    var yValues = [[Double]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.whiteColor()
        
        for lc in lineCharts {
//            lc.backgroundColor = UIColor.lightGrayColor()
            lc.descriptionText = ""
            lc.xAxis.labelPosition = .Bottom
            lc.xAxis.drawLabelsEnabled = true
            lc.xAxis.drawGridLinesEnabled = false
            lc.leftAxis.drawGridLinesEnabled = false
            lc.rightAxis.drawGridLinesEnabled = false
            
            lc.xAxis.valueFormatter = xFormatter
            lc.leftAxis.valueFormatter = yFormatter
            lc.rightAxis.valueFormatter = yFormatter
            
            lc.leftAxis.axisMinimum = displayMinimum
            lc.leftAxis.axisMaximum = displayMaximum
            
            lc.rightAxis.axisMinimum = displayMinimum
            lc.rightAxis.axisMaximum = displayMaximum
        }
        
        clearGraph()

    }

    override func viewWillAppear(animated: Bool) {
        resetStartButton()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func clearGraph() {
        let windowSize = Telemetry.windowSize
        
        xValues.removeAll()
        yValues.removeAll()
        
        for _ in 0..<numOfCharts {
            
            var xs = [Double]()
            var ys = [Double]()
            
            let xInterval = 1.0/Double(displaySamplingFrequency)
            
            for i in 0..<windowSize*displaySamplingFrequency {
                xs.append(-Double(windowSize) + xInterval*Double(i))
                ys.append(0)
            }
            
            xValues.append(xs)
            yValues.append(ys)
        }
        
        let displayIndices = getOrderedDisplayIndices()
        
        let baseDisplayChannel = Telemetry.channelSource.0
        
        for i in 0..<numOfCharts {
            setChart(&xValues[i], ys: &yValues[i], chartPosition:i, channelIndex:baseDisplayChannel + displayIndices[i])
        }
    }
    
    func getRepeatingCount(inout data:[UInt8], startBitIndex:Int) -> Int {
        
        let firstBit = getBit(&data, bitIndex: startBitIndex)

        var bitIndex = startBitIndex
        var bit = getBit(&data, bitIndex: bitIndex)
        
        while (bit == firstBit) {
            bitIndex += 1
            
            if bitIndex == data.count * 8 {
                break
            }
            
            bit = getBit(&data, bitIndex: bitIndex)
        }
        
        if bitIndex == data.count * 8 {
            return -1
        } else {
            return bitIndex - startBitIndex
        }
    }
    
    func getBit(inout data:[UInt8], bitIndex:Int) -> Bool {
        
        let byteIndex = bitIndex >> 3
        let byteOffset = (UInt8)(bitIndex & 7)
        let bitMask = (UInt8)(0x1 << (7-byteOffset))
        
        return (data[byteIndex] & bitMask) > 0
    }
    
    func setBit(inout data:[UInt8], bitIndex:Int, value:Bool) {
        
        let byteIndex = bitIndex >> 3
        let byteOffset = (UInt8)(bitIndex & 7)
        let bitMask = UInt8(0b10000000 >> byteOffset)
        
        if data.count == byteIndex {
            data.append(0)
        }
        
        if value {
            data[byteIndex] |= bitMask
        }
    }
    
    func decodeBits(inout rawData:[UInt8]) {
        
        rawBitStream += rawData
        
        while rawBitStreamIndex < (rawBitStream.count * 8) {
            
            let repeatingCount = getRepeatingCount(&rawBitStream, startBitIndex: rawBitStreamIndex)
            
            if repeatingCount == -1 {
                break
            } else {
                
                let repeatingBit = getBit(&rawBitStream, bitIndex: rawBitStreamIndex)
                
                let numOfDecodedBits = repeatingCount / oversamplingFactor
                
                for _ in 0..<numOfDecodedBits {
                    setBit(&decodedBitStream, bitIndex: decodedBitStreamIndex, value: repeatingBit)
                    decodedBitStreamIndex += 1
                }
                
                rawBitStreamIndex += repeatingCount
            }
        }
    }
    
    func getDecodedValue(inout data:[UInt8], startBitIndex:Int, endBitIndex:Int) -> UInt16 {
        
        var value:UInt16 = 0
        
        for i in startBitIndex..<endBitIndex {
            
            value <<= 1
            
            if getBit(&data, bitIndex: i) {
                value += 1
            }
        }
        
        value &= 0x3FF // clip upper two bits
        
        return value
    }
    
    func processBits(inout samples:[UInt16]) {
        
        var startBitIndex = decodedIndex
        var endBitIndex = decodedIndex + numOfBitsPerSample
        
        while endBitIndex <= decodedBitStreamIndex {
            
            let value = getDecodedValue(&decodedBitStream, startBitIndex: startBitIndex, endBitIndex: endBitIndex)

            samples.append(value)
            
            decodedIndex = endBitIndex
            startBitIndex = endBitIndex
            endBitIndex += numOfBitsPerSample
        }
    }
    
    func getOrderedDisplayIndices() -> [Int] {
        var displayIndices = Telemetry.channelDisplayIndices
        
        if displayIndices[0] > displayIndices[1] {
            let tmp = displayIndices[0]
            displayIndices[0] = displayIndices[1]
            displayIndices[1] = tmp
        }
        
        return displayIndices
    }
    
    func displayData(inout samples:[UInt16]) -> Bool {
        var needRefresh = false

        if samples.count > displayIndex + displayInterval {
            let numOfChannels = Telemetry.numOfChannels
            let displayIndices = getOrderedDisplayIndices()
            let windowSize = Telemetry.windowSize
            
            needRefresh = true
            
            let xInterval = 1.0 / Double(displaySamplingFrequency)
            let undersampleFactor = samplingFrequencyMCU / displaySamplingFrequency
            
            var chartPosition = 0
            for channelDisplayIndex in displayIndices {
                
                var newXs = [Double]()
                var newYs = [Double]()
                
                for i in (displayIndex + channelDisplayIndex).stride(to:displayIndex + displayInterval + channelDisplayIndex, by:undersampleFactor * numOfChannels) {
                    newXs.append(Double(i/(undersampleFactor * numOfChannels)) * xInterval)
                    newYs.append(Double(samples[i]) / 1023.0 * (expectedMaximum-expectedMinimum) + expectedMinimum)
                }
                
                // shift points to the left
                let numOfDisplayPoints = windowSize * displaySamplingFrequency
                self.yValues[chartPosition] = self.yValues[chartPosition][newYs.count..<numOfDisplayPoints] + newYs
                self.xValues[chartPosition] = self.xValues[chartPosition][newXs.count..<numOfDisplayPoints] + newXs
                
                chartPosition += 1
            }
            displayIndex += displayInterval
        }

        return needRefresh
    }
    
    var foo = [UInt16]()
    

    func saveAllData(inout dataSamplesToSave: [UInt16]) {
        
        print ("saving \(dataSamplesToSave.count)")
        
        let telemetryEntity = NSEntityDescription.entityForName("TelemetryData", inManagedObjectContext: managedObjectContext)
        let channelEntity = NSEntityDescription.entityForName("ChannelData", inManagedObjectContext: managedObjectContext)
        let dataPointEntity = NSEntityDescription.entityForName("ChannelDataPoint", inManagedObjectContext: managedObjectContext)
        
        let newTelemetry = NSManagedObject(entity: telemetryEntity!, insertIntoManagedObjectContext: managedObjectContext) as! TelemetryData
        
        newTelemetry.basechannel = Telemetry.channelSource.0
        newTelemetry.topchannel = Telemetry.channelSource.1
        newTelemetry.samplingfrequency = Telemetry.samplingFrequency
        newTelemetry.timestamp = NSDate()
        
        let numOfChannels = Telemetry.numOfChannels
        
        let skipFactor = Telemetry.maxSamplingFrequency / Telemetry.samplingFrequency
        
        let channels = newTelemetry.mutableOrderedSetValueForKey("channels")
        
        for c in 0..<numOfChannels {

            let newChannel = NSManagedObject(entity: channelEntity!, insertIntoManagedObjectContext: managedObjectContext) as! ChannelData
            newChannel.channelindex = c
            
            let dataPoints = newChannel.mutableOrderedSetValueForKey("datapoints")
            
            var i = c
            
            while i < dataSamplesToSave.count {
                
                let newDataPoint = NSManagedObject(entity: dataPointEntity!, insertIntoManagedObjectContext: managedObjectContext) as! ChannelDataPoint
                let dataIndex = (i / numOfChannels)
                let yValue = Int(dataSamplesToSave[i])
                
                newDataPoint.x = dataIndex
                newDataPoint.y = yValue
                
                dataPoints.addObject(newDataPoint)
                
                i += (numOfChannels * skipFactor)
            }
            
            channels.addObject(newChannel)
        }
        
        do {
            try managedObjectContext.save()
        } catch {
            print(error)
        }
    }
    
    var next = 1000
    var interval = 1000
    
    
    @IBAction func startRecording(sender: UIBarButtonItem) {
        
        if streaming {
            // stop streaming
            streaming = false
            resetStartButton()
            
        } else {
            clearGraph()
            
            recording = Telemetry.displayModeIndex == 1
            
            streaming = true
            sender.image = UIImage(named:"Stop") // change icon
            
            rawBitStreamIndex = 0 // how much has been processed
            rawBitStream = [UInt8]()
            
            decodedBitStreamIndex = 0 // where to add next bit
            decodedIndex = 0 // how much has been converted into a value
            decodedBitStream = [UInt8]()
            
            displayIndex = 0
            saveIndex = 0
            
            needToSave = false
            
            let baseChannel = Telemetry.channelSource.0
            let orderedDisplayIndices = getOrderedDisplayIndices()
            
            let qos = Int(QOS_CLASS_USER_INTERACTIVE.rawValue)
            dispatch_async(dispatch_get_global_queue(qos, 0)) { () -> Void in

                var (success, errmsg) = Util.client.connect(timeout: 10)
                
                if success {
                
                    var dataSamplesReceived = [UInt16]()
                    
                    print ("sending message")
                    
                    let message = "t\(Telemetry.channelSourceIndex+1)" // add 1 since 0 for MCU means none selected
                    (success, errmsg) = Util.client.send(str:message)
                    
                    if success {
                        
                        var data = Util.client.read(1024)
                        
                        while self.streaming && data != nil {
                            
                            self.decodeBits(&data!)
                            self.processBits(&dataSamplesReceived)

                            let needRefresh = self.displayData(&dataSamplesReceived)
                            if needRefresh {
                                dispatch_async(dispatch_get_main_queue()) {
                                    for i in 0..<self.numOfCharts {
                                        self.setChart(&self.xValues[i], ys: &self.yValues[i], chartPosition: i, channelIndex: baseChannel + orderedDisplayIndices[i])
                                    }
                                }
                            }
                            
                            data = Util.client.read(1024)
                        }
                        
                        if self.recording { // record date only if connection was successful
                            dispatch_async(dispatch_get_main_queue()) {
                                self.dataSamples = dataSamplesReceived
                                self.needToSave = true
                                self.saveAllData(&self.dataSamples)
                                
                                let alertController = UIAlertController(title: "", message:
                                    "Telemetry data saved", preferredStyle: UIAlertControllerStyle.Alert)
                                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                        }
                    } else {
                        print (errmsg)
                    }
                } else {
                    print(errmsg)
                }
                
                Util.client.close()
                
                self.streaming = false
                dispatch_async(dispatch_get_main_queue()) {
                    self.resetStartButton()
                    
                    if !success {
                        let alertController = UIAlertController(title: "", message:
                            "Unable to connect to rendezvous device. Please try again later.", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func resetStartButton() {
        if Telemetry.displayModeIndex == 0 {
            startButton.image = UIImage(named:"Play")
        } else {
            startButton.image = UIImage(named:"VideoFilled")
        }
    }
    
    func setChart(inout xs: [Double],inout ys: [Double], chartPosition:Int, channelIndex:Int) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<xs.count {
            let dataEntry = ChartDataEntry(x: xs[i] , y: ys[i])
            dataEntries.append(dataEntry)
        }
        
        let red = Double(0x34) // 00, 9e, 60
        let green = Double(0x84)
        let blue = Double(0xbf)
        let color = UIColor(red: CGFloat(red/255.0), green: CGFloat(green/255.0), blue: CGFloat(blue/255.0), alpha: 1)
        //let color = UIColor.redColor()
        
        var colors: [UIColor] = []
        colors.append(color)
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Channel \(channelIndex+1) Neural Response (V)")
        lineChartDataSet.colors = colors
        lineChartDataSet.drawCirclesEnabled = false
        
        //let stringDataPoints = dataPoints.map {"\(Double($0) / samplingFrequency)s"}
        let lineChartData = LineChartData(dataSets: [lineChartDataSet])
        
        lineCharts[chartPosition].data = lineChartData
        lineChartData.setDrawValues(false)
        
        //lineCharts[i].xAxis.setLabelsToSkip(Int(samplingFrequency/2.0) - 1)
        //lineCharts[i].animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
    }

    func createTelemetryModel() -> [Parameter] {
        var toReturn = [Parameter]()
        
        let modeParameter = Parameter.Picker(type: .DisplayMode, values: ["Live feed", "Recording"])
        toReturn.append(modeParameter)
        
        let channelSources = Telemetry.channelSources.map{"\($0.0 + 1)-\($0.1 + 1)"}
        let channelParameter = Parameter.Picker(type: .ChannelSource, values: channelSources)
        toReturn.append(channelParameter)
        
        let windowParameter = Parameter.Picker(type: .WindowSize, values: Telemetry.windowSizes.map{"\($0) s"})
        toReturn.append(windowParameter)
        
        let samplingParameter = Parameter.Picker(type: .SamplingFrequency, values: Telemetry.samplingFrequencies.map {"\($0/1000) KSa/s"})
        toReturn.append(samplingParameter)
    
        return toReturn
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        clearGraph()
    }
    
    func createChannelDisplayModel() -> Parameter {
        
        let index = Util.getSelectedIndex(.Telemetry, parameterType: .ChannelSource)
        let range = Telemetry.channelSources[index]
        let minChannel = range.0
        let maxChannel = range.1
        
        var values = [String]()
        
        for i in minChannel...maxChannel {
            values.append("Channel \(i+1)")
        }
        
        return .Picker(type: .ChannelDisplay, values: values)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier != nil {
            switch segue.identifier! {
            case "showTelemetrySettings":
                if let destination = segue.destinationViewController as? SettingsTableViewController {
                    if sender as? UIBarButtonItem != nil {
                        destination.navigationItem.title = "Telemetry Settings"
                        destination.model = createTelemetryModel()
                        destination.pageType = .Telemetry
                    }
                }
            case "showChannelDisplays":
                if let destination = segue.destinationViewController as? ChannelDisplayViewController {
                    if sender as? UIBarButtonItem != nil {
                        destination.navigationItem.title = "Channel Display"
                        if let ppc = destination.popoverPresentationController {
                            ppc.delegate = self
                        }
                    }
                    destination.model = createChannelDisplayModel()
                }
            default:
                break
            }
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
}
